"use strict";
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var config = require('../config');
var utils = require('../utils');
var plugins = gulpLoadPlugins();
var INLINE_OPTIONS = {
    base: config.TMP_DIR,
    useRelativePaths: true,
    removeLineBreaks: true
};

module.exports = function () {
    var tsProject = utils.makeTsProject();
    var src = [
        'typings/index.d.ts',
        config.TOOLS_DIR + '/manual_typings/**/*.d.ts',
        path.join(config.TMP_DIR, '**/*.ts')
    ];
    var result = gulp.src(src)
        .pipe(plugins.plumber())
        .pipe(plugins.inlineNg2Template(INLINE_OPTIONS))
        .pipe(plugins.typescript(tsProject))
        .once('error', function () {
        this.once('finish', function () { return process.exit(1); });
    });
    return result.js
        .pipe(plugins.template(utils.templateLocals()))
        .pipe(gulp.dest(config.TMP_DIR));
};
