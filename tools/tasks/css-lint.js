"use strict";

var colorguard = require('colorguard');
var doiuse = require('doiuse');
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var merge = require('merge-stream');
var reporter = require('postcss-reporter');
var stylelint = require('stylelint');
var path = require('path');
var config = require('../config');
var plugins = gulpLoadPlugins();
var isProd = config.ENV === 'prod';
var stylesheetType = config.ENABLE_SCSS ? 'scss' : 'css';
var processors = [
    doiuse({
        browsers: config.BROWSER_LIST,
    }),
    colorguard({
        whitelist: config.COLOR_GUARD_WHITE_LIST
    }),
    stylelint(),
    reporter({ clearMessages: true })
];

function lintComponentStylesheets() {
    return gulp.src([
        path.join(config.APP_SRC, '**', "*." + stylesheetType),
        ("!" + path.join(config.APP_SRC, 'assets', '**', '*.scss')),
        ("!" + path.join(config.CSS_SRC, '**', '*.css'))
    ]).pipe(isProd ? plugins.cached('css-lint') : plugins.util.noop())
        .pipe(config.ENABLE_SCSS ? plugins.sassLint() : plugins.postcss(processors))
        .pipe(config.ENABLE_SCSS ? plugins.sassLint.format() : plugins.util.noop())
        .pipe(config.ENABLE_SCSS ? plugins.sassLint.failOnError() : plugins.util.noop());
}
function lintExternalStylesheets() {
    var m = getExternalStylesheets().map(function (r) { return r.src; });
    if (m.length > 0) {
        return gulp.src(m)
            .pipe(isProd ? plugins.cached('css-lint') : plugins.util.noop())
            .pipe(config.ENABLE_SCSS ? plugins.sassLint() : plugins.postcss(processors))
            .pipe(config.ENABLE_SCSS ? plugins.sassLint.format() : plugins.util.noop())
            .pipe(config.ENABLE_SCSS ? plugins.sassLint.failOnError() : plugins.util.noop());
    }
    else {
        return [];
    }
}
function getExternalStylesheets() {
    var stylesheets = config.ENABLE_SCSS ? config.DEPENDENCIES : config.APP_ASSETS;
    return stylesheets
        .filter(function (d) { return new RegExp("." + stylesheetType + "$")
        .test(d.src) && !d.vendor; });
}

module.exports = function () { return merge(lintComponentStylesheets(), lintExternalStylesheets()); };