"use strict";

var gulp = require('gulp');
var path = require('path');
var config = require('../config');

module.exports = function () {
    return gulp.src([
        path.join(config.APP_SRC, '**/*.ts'),
        '!' + path.join(config.APP_SRC, '**/*.spec.ts'),
        '!' + path.join(config.APP_SRC, '**/*.e2e-spec.ts')
    ])
        .pipe(gulp.dest(config.TMP_DIR));
};
