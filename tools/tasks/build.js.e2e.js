"use strict";
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var config = require('../config');
var utils = require('../utils');
var plugins = gulpLoadPlugins();
module.exports = function () {
    var tsProject = utils.makeTsProject();
    var src = [
        'typings/index.d.ts',
        config.TOOLS_DIR + '/manual_typings/**/*.d.ts',
        path.join(config.APP_SRC, '**/*.ts'),
        '!' + path.join(config.APP_SRC, '**/*.spec.ts')
    ];
    var result = gulp.src(src)
        .pipe(plugins.plumber())
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.typescript(tsProject));
    return result.js
        .pipe(plugins.sourcemaps.write())
        .pipe(plugins.template(utils.templateLocals()))
        .pipe(gulp.dest(config.APP_DEST));
};
