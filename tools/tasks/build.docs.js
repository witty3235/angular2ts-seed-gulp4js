"use strict";

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var config = require('../config');
var plugins = gulpLoadPlugins();

module.exports = function () {
    var src = [
        'typings/index.d.ts',
        path.join(config.APP_SRC, '**/*.ts'),
        '!' + path.join(config.APP_SRC, '**/*.spec.ts'),
        '!' + path.join(config.APP_SRC, '**/*.e2e-spec.ts')
    ];
    return gulp.src(src)
        .pipe(plugins.typedoc({
            module: 'commonjs',
            target: 'es5',
            excludeExternals: true,
            includeDeclarations: true,
            out: config.DOCS_DEST,
            json: path.join(config.DOCS_DEST, 'data/docs.json'),
            name: config.APP_TITLE,
            ignoreCompilerErrors: false,
            experimentalDecorators: true,
            version: true
        }));
};
