"use strict";
var express = require('express');
var history = require('express-history-api-fallback');
var gulp = require('gulp');
var path = require('path');
var gulp_protractor = require('gulp-protractor');

var Protractor = (function () {
    function Protractor() {
    }
    Protractor.prototype.server = function (port, dir) {
        var app = express();
        var root = path.resolve(process.cwd(), dir);
        app.use(express.static(root));
        app.use(history('index.html', { root: root }));
        return new Promise(function (resolve, reject) {
            var server = app.listen(port, function () {
                resolve(server);
            });
        });
    };
    return Protractor;
}());

module.exports = function (done) {
    new Protractor()
        .server(5555, './dist/prod')
        .then(function (server) {
        gulp
            .src('./dist/dev/**/*.e2e-spec.js')
            .pipe(gulp_protractor.protractor({ configFile: 'protractor.conf.js' }))
            .on('error', function (error) { throw error; })
            .on('end', function () { server.close(done); });
    });
};
