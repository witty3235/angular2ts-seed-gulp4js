"use strict";
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var merge = require('merge-stream');
var config = require('../config');
var plugins = gulpLoadPlugins();
function getShims() {
    var libs = config.DEPENDENCIES
        .filter(function (d) { return /\.js$/.test(d.src); });
    return libs.filter(function (l) { return l.inject === 'shims'; })
        .concat(libs.filter(function (l) { return l.inject === 'libs'; }))
        .concat(libs.filter(function (l) { return l.inject === true; }))
        .map(function (l) { return l.src; });
}
function bundleShims() {
    return gulp.src(getShims())
        .pipe(plugins.uglify({
        mangle: false
    }))
        .pipe(plugins.concat(config.JS_PROD_SHIMS_BUNDLE))
        .pipe(plugins.replace(/('|")use strict\1;var Reflect;/, 'var Reflect;'))
        .pipe(gulp.dest(config.JS_DEST));
}
module.exports = function () { return merge(bundleShims()); };
