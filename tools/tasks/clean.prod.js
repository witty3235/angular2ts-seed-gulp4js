"use strict";

var config = require('../config');
var utils = require('../utils');

module.exports = utils.clean([config.PROD_DEST, config.TMP_DIR]);
