"use strict";

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var slash = require('slash');
var config = require('../config');
var utils = require('../utils');
var plugins = gulpLoadPlugins();

function inject(name) {
    return plugins.inject(gulp.src(getInjectablesDependenciesRef(name), { read: false }), {
        name: name,
        transform: transformPath()
    });
}
function getInjectablesDependenciesRef(name) {
    return config.DEPENDENCIES
        .filter(function (dep) { return dep['inject'] && dep['inject'] === (name || true); })
        .map(mapPath);
}
function mapPath(dep) {
    var envPath = dep.src;
    if (envPath.startsWith(config.APP_SRC) && !envPath.endsWith('.scss')) {
        envPath = path.join(config.APP_DEST, envPath.replace(config.APP_SRC, ''));
    }
    else if (envPath.startsWith(config.APP_SRC) && envPath.endsWith('.scss')) {
        envPath = envPath.replace(config.ASSETS_SRC, config.CSS_DEST).replace('.scss', '.css');
    }
    return envPath;
}
function transformPath() {
    return function (filepath) {
        arguments[0] = path.join(config.APP_BASE, filepath) + ("?" + Date.now());
        return slash(plugins.inject.transform.apply(plugins.inject.transform, arguments));
    };
}

module.exports = function () {
    return gulp.src(path.join(config.APP_SRC, 'index.html'))
        .pipe(inject('shims'))
        .pipe(inject('libs'))
        .pipe(inject())
        .pipe(plugins.template(utils.templateLocals()))
        .pipe(gulp.dest(config.APP_DEST));
};
