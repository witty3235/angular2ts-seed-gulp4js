"use strict";

var config = require('../config');
var utils = require('../utils');

module.exports = utils.clean([config.DIST_DIR, 'coverage', 'docs']);
