"use strict";

var karma = require('karma');
var path = require('path');

module.exports = function (done) {
    new karma.Server({
        configFile: path.join(process.cwd(), 'karma.conf.js'),
        singleRun: true
    }).start(done);
};
