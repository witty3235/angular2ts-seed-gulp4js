"use strict";

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var slash = require('slash');
var config = require('../config');
var utils = require('../utils');
var plugins = gulpLoadPlugins();

function inject() {
    var files = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        files[_i - 0] = arguments[_i];
    }
    return plugins.inject(gulp.src(files, { read: false }), {
        files: files,
        transform: transformPath()
    });
}
function injectJs() {
    return inject(path.join(config.JS_DEST, config.JS_PROD_SHIMS_BUNDLE), path.join(config.JS_DEST, config.JS_PROD_APP_BUNDLE));
}
function injectCss() {
    return inject(path.join(config.CSS_DEST, config.CSS_PROD_BUNDLE));
}
function transformPath() {
    return function (filepath) {
        var path1 = path.normalize(filepath).split(path.sep);
        arguments[0] = config.APP_BASE + path1.slice(3, path1.length).join(path.sep) + ("?" + Date.now());
        return slash(plugins.inject.transform.apply(plugins.inject.transform, arguments));
    };
}

module.exports = function () {
    return gulp.src(path.join(config.APP_SRC, 'index.html'))
        .pipe(injectJs())
        .pipe(injectCss())
        .pipe(plugins.template(utils.templateLocals()))
        .pipe(gulp.dest(config.APP_DEST));
};
