"use strict";
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var config = require('../config');
var utils = require('../utils');
var plugins = gulpLoadPlugins();
module.exports = function () {
    var tsProject = utils.makeTsProject();
    var src = [
        'typings/index.d.ts',
        config.TOOLS_DIR + '/manual_typings/**/*.d.ts',
        path.join(config.APP_SRC, '**/*.ts'),
        '!' + path.join(config.APP_SRC, '**/*.e2e-spec.ts'),
        '!' + path.join(config.APP_SRC, config.BOOTSTRAP_MODULE + ".ts")
    ];
    var result = gulp.src(src)
        .pipe(plugins.plumber())
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.inlineNg2Template({
            base: config.APP_SRC,
            useRelativePaths: true,
            supportNonExistentFiles: config.ENABLE_SCSS
        }))
        .pipe(plugins.typescript(tsProject));
    return result.js
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(config.APP_DEST));
};
