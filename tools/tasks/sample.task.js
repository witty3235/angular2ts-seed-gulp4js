"use strict";

var gulp = require('gulp');
var path = require('path');
var config = require('../config');

module.exports = function () {
    return gulp.src(path.join(config.APP_SRC, '**/*.ts'))
        .pipe(gulp.dest(config.APP_DEST));
};
