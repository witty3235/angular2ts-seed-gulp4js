"use strict";
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var merge = require('merge-stream');
var path = require('path');
var config = require('../config');
var plugins = gulpLoadPlugins();
var cleanCss = require('gulp-clean-css');
var processors = [
    autoprefixer({
        browsers: config.BROWSER_LIST
    })
];
var isProd = config.ENV === 'prod';
console.log(isProd);
if (isProd) {
    processors.push(cssnano({
        discardComments: { removeAll: true },
        discardUnused: false,
        zindex: false,
        reduceIdents: false
    }));
}

function prepareTemplates() {
    return gulp.src(path.join(config.APP_SRC, '**', '*.html'))
        .pipe(gulp.dest(config.TMP_DIR));
}

function processComponentStylesheets() {
    return config.ENABLE_SCSS ? processComponentScss() : processComponentCss();
}

function processComponentScss() {
    return gulp.src(path.join(config.APP_SRC, '**', '*.scss'))
        .pipe(isProd ? plugins.cached('process-component-scss') : plugins.util.noop())
        .pipe(isProd ? plugins.progeny() : plugins.util.noop())
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass({ includePaths: ['./node_modules/'] }).on('error', plugins.sass.logError))
        .pipe(plugins.postcss(processors))
        .pipe(plugins.sourcemaps.write(isProd ? '.' : ''))
        .pipe(gulp.dest(isProd ? config.TMP_DIR : config.APP_DEST));
}

function processComponentCss() {
    return gulp.src([
        path.join(config.APP_SRC, '**', '*.css'),
        '!' + path.join(config.APP_SRC, 'assets', '**', '*.css')
    ])
        .pipe(isProd ? plugins.cached('process-component-css') : plugins.util.noop())
        .pipe(plugins.postcss(processors))
        .pipe(gulp.dest(isProd ? config.TMP_DIR : config.APP_DEST));
}

function processExternalStylesheets() {
    return config.ENABLE_SCSS ? processAllExternalStylesheets() : processExternalCss();
}

function processAllExternalStylesheets() {
    return merge(getExternalCssStream(), getExternalScssStream())
        .pipe(isProd ? plugins.concatCss(config.CSS_PROD_BUNDLE) : plugins.util.noop())
        .pipe(plugins.postcss(processors))
        .pipe(isProd ? cleanCss() : plugins.util.noop())
        .pipe(gulp.dest(config.CSS_DEST));
}

function getExternalCssStream() {
    var m = getExternalCss();
    if(m.length > 0) {
        return gulp.src(m)
            .pipe(isProd ? plugins.cached('process-external-css') : plugins.util.noop());
    } else {
        return [];
    }

}

function getExternalCss() {
    return config.DEPENDENCIES.filter(function (dep) { return /\.css$/.test(dep.src); }).map(function (dep) { return dep.src; });
}

function getExternalScssStream() {
    var m = getExternalScss();
    if(m.length > 0) {
        return gulp.src(getExternalScss())
            .pipe(isProd ? plugins.cached('process-external-scss') : plugins.util.noop())
            .pipe(isProd ? plugins.progeny() : plugins.util.noop())
            .pipe(plugins.sass({ includePaths: ['./node_modules/'] }).on('error', plugins.sass.logError));
    } else {
        return [];
    }
}

function getExternalScss() {
    return config.DEPENDENCIES.filter(function (dep) { return /\.scss$/.test(dep.src); }).map(function (dep) { return dep.src; })
        .concat([path.join(config.CSS_SRC, '**', '*.scss')]);
}

function processExternalCss() {
    return getExternalCssStream()
        .pipe(plugins.postcss(processors))
        .pipe(isProd ? plugins.concatCss(config.CSS_PROD_BUNDLE) : plugins.util.noop())
        .pipe(isProd ? cleanCss() : plugins.util.noop())
        .pipe(gulp.dest(config.CSS_DEST));
}

module.exports = function () { return merge(processComponentStylesheets(), prepareTemplates(), processExternalStylesheets()); };
