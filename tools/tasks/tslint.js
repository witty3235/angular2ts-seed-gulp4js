"use strict";
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var config = require('../config');
var plugins = gulpLoadPlugins();
module.exports = function () {
    var src = [
        path.join(config.APP_SRC, '**/*.ts'),
        '!' + path.join(config.APP_SRC, '**/*.d.ts'),
        path.join(config.TOOLS_DIR, '**/*.ts'),
        '!' + path.join(config.TOOLS_DIR, '**/*.d.ts')
    ];
    return gulp.src(src)
        .pipe(plugins.tslint({
            rulesDirectory: config.CODELYZER_RULES
        }))
        .pipe(plugins.tslint.report(require('tslint-stylish'), {
            emitError: require('is-ci'),
            sort: true,
            bell: true
        }));
};
