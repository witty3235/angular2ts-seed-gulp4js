"use strict";
var path = require('path');
var Builder = require('systemjs-builder');
var config = require('../config');
var BUNDLER_OPTIONS = {
    format: 'cjs',
    minify: true,
    mangle: false
};
module.exports = function (done) {
    var builder = new Builder(config.SYSTEM_BUILDER_CONFIG);
    builder
        .buildStatic(path.join(config.TMP_DIR, config.BOOTSTRAP_MODULE), path.join(config.JS_DEST, config.JS_PROD_APP_BUNDLE), BUNDLER_OPTIONS)
        .then(function () { return done(); })
        .catch(function (err) { return done(err); });
};
