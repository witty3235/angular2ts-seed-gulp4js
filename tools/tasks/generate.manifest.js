"use strict";

var gulp = require('gulp');
var config = require('../config');

module.exports = function () {
    return require('angular2-service-worker')
        .gulpGenManifest({
        group: [{
                name: 'css',
                sources: gulp.src(config.APP_DEST + "/**/*.css")
            }, {
                name: 'js',
                sources: gulp.src(config.APP_DEST + "/**/*.js")
            }]
    })
        .pipe(gulp.dest(config.APP_DEST));
};
