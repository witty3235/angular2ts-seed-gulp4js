"use strict";
var gulp = require('gulp');
var path = require('path');
var config = require('../config');
var es = require('event-stream');
var onlyDirs = function (es) {
    return es.map(function (file, cb) {
        if (file.stat.isFile()) {
            return cb(null, file);
        }
        else {
            return cb();
        }
    });
};
module.exports = function () {
    return gulp.src([
        path.join(config.APP_SRC, '**'),
        '!' + path.join(config.APP_SRC, '**', '*.ts'),
        '!' + path.join(config.APP_SRC, '**', '*.css'),
        '!' + path.join(config.APP_SRC, '**', '*.html'),
        '!' + path.join(config.APP_SRC, '**', '*.scss'),
        '!' + path.join(config.ASSETS_SRC, '**', '*.js')
    ].concat(config.TEMP_FILES.map(function (p) { return '!' + p; })))
        .pipe(onlyDirs(es))
        .pipe(gulp.dest(config.APP_DEST));
};
