"use strict";

var gulp = require('gulp');
var path = require('path');
var config = require('../config');

module.exports = function () {
    var paths = [
        path.join(config.APP_SRC, '**'),
        '!' + path.join(config.APP_SRC, '**', '*.ts'),
        '!' + path.join(config.APP_SRC, '**', '*.scss')
    ].concat(config.TEMP_FILES.map(function (p) { return '!' + p; }));
    return gulp.src(paths)
        .pipe(gulp.dest(config.APP_DEST));
};
