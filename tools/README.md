# Tools documentation

This document contains information about the tools section of the `angular2ts-seed-gulp4js`.

## General Information

The root of this folder contains the following files:

| Filename     | Description |
| :----------- | :---------- |
| `.gitignore` | Adds the generated `*.js` and `.js.map` files to the list of ignores files for git |
| `config.js`  | Exports the project configuration, which contains of the basic configuration provided by `/config/seed.config.js` and the project specific overrides defined by `/config/project.config.js` |
| `debug.js`   | Provides the ability to debug a specific build task |
| `README.md`  | The documentation of the tools section |
| `utils.js`   | Exports the utilities provided by the seed barrel file (`/utils/seed.utils.js`) and the project specific barrel file (`/utils/project.utils.js`) |

The subfolders provide further folders to distinguish between files which are provided by the seed (located in the corresponding `seed` folder) and files which can be specific by project (to be located in the corresponding `project` folder). This helps you to include updates from the `angular2ts-seed-gulp4js` without causing conflicts with you personal customisations.

## Configuration

The configuration contains of a default configuration provided by `/config/config.default.js` file. You can add your own custom configuration within the `/config/config.js` file, which extends the seed configuration.

## Manual Typings

The `manual_typings` folder contains of manual TypeScript typings. As for the project specific typings there is a sample provided (`/manual_typings/sample.package.d.ts`) to help you get started.

## Tasks

The `tasks` folder contains of gulp tasks. As for the project specific tasks there is a sample provided (`/tasks/sample.task.js`) to help you get started.

The seed provides the following tasks:

| Filename               | Description |
| :--------------------- | :---------- |
| `build.assets.dev.js`  | Copies the assets (located in `src/client/assets`) over to the `dist/dev/assets` directory |
| `build.assets.prod.js` | Copies the assets (located in `src/client/assets`) over to the `dist/prod/assets` directory |
| `build.bundles.app.js` | Bundles the JavaScript files using the SystemJS Builder |
| `build.bundles.js`     | Bundles the JavaScript shim dependencies |
| `build.docs.js`        | Builds the documentation for the TypeScript files using `typedoc` |
| `build.html_css.js`    | Builds the `html` and `css` files and applies CSS postprocessing |
| `build.index.dev.js`   | Builds the `index.html` for the `dev` environment |
| `build.index.prod.js`  | Builds the `index.html` for the `prod` environment |
| `build.js.dev.js`      | Transpiles the TypeScript files (excluding specs and e2e specs) for the `dev` environment |
| `build.js.e2e.js`      | Transpiles the TypeScript files (excluding specs and e2e specs) for the `e2e` environment |
| `build.js.prod.js`     | Transpiles the TypeScript files (excluding specs and e2e specs) for the `prod` environment |
| `build.js.test.js`     | Transpiles the TypeScript files (excluding specs and e2e specs) for the `test` environment |
| `check.versions.js`    | Checks if the required Node and NPM (as defined in `/config/seed.config.js`) are installed |
| `clean.all.js`         | Cleans all files within the `/dist` directory |
| `clean.dev.js`         | Cleans all files within the `/dist/dev` directory |
| `clean.prod.js`        | Cleans all files within the `/dist/prod` directory |
| `copy.js.prod.js`      | Copies all TypeScript files (excluding specs and e2e specs) over to the `/tmp` dir |
| `css-lint.js`          | Lints all `css` files using `stylelint` |
| `e2e.js`               | Runs all e2e specs using `protractor` |
| `generate.manifest.js` | Generates a `manifest` file for the application |
| `karma.start.js`       | Starts the unit tests using `karma` |
| `serve.coverage.js`    | Serves the unit test coverage report using an `express` server |
| `serve.docs.js`        | Serves the application documentation using an `express` server |
| `serve.prod.js`        | Serves the files from `/dist/prod` using an `express` server |
| `serve.start.js`       | Serves the files from `/dist/dev` using an `express` server |
| `tslint.js`            | Lints the TypeScript files using `codelyzer` |
| `watch.dev.js`         | Watches for code changes and rebuilds the files in `/dist/dev` |
| `watch.e2e.js`         | Watches for code changes and rebuilds the files in `/dist/e2e` |
| `watch.test.js`        | Watches for code changes and rebuilds the files in `/dist/test` |
| `webdriver.js`         | Installs the Selenium webdriver used for the Protractor e2e specs |

## Utilities

The `utils` folder contains of utilities. As for the project specific utilities there is a sample provided (`/utils/sample_util.js`) to help you get started.

The utilities are exported by the barrel files `utils.js`.

The seed provides the following utilities:

| Filename               | Description |
| :--------------------- | :---------- |
| `clean.js`             | Provides a utility to clean files and directories |
| `code_change_tools.js` | Provides utilities to make use of BrowserSync to refresh the browser after a code change |
| `server.js`            | Provides utilities to start `express` servers for the application, the documentation and the unit test coverage |
| `task_tools.js`        | Provides utilities to start tasks (matching task names as string input parameters from the `gulpfile.js` to the corresponding files) |
| `tsproject.js`         | Provides a utility to configure the TypeScript transpilation |
| `watch.js`             | Provides a utility to watch for file changes and notify live reloads |
