"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};

var path = require('path');
var configDefault = require('./config.default');
var ProjectConfig = (function (_super) {
    __extends(ProjectConfig, _super);
    function ProjectConfig() {
        _super.call(this);
        this.PROJECT_TASKS_DIR = path.join(process.cwd(), this.TOOLS_DIR, 'tasks');
        this.NPM_DEPENDENCIES = this.NPM_DEPENDENCIES.slice();
        this.APP_ASSETS = this.APP_ASSETS.slice();
    }
    return ProjectConfig;
}(configDefault.ConfigDefault));

exports.ProjectConfig = ProjectConfig;
