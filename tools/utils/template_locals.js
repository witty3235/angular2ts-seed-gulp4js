"use strict";

var yargs = require('yargs');
var CONFIG = require('../config');

function templateLocals() {
    return CONFIG;
}

exports.templateLocals = templateLocals;
