"use strict";

var express = require('express');
var fallback = require('express-history-api-fallback');
var openResource = require('open');
var path = require('path');
var serveStatic = require('serve-static');
var codeChangeTool = require('./code_change_tools');
var config = require('../config');

function serveSPA() {
    codeChangeTool.listen();
}
exports.serveSPA = serveSPA;

function notifyLiveReload(e) {
    var fileName = e.path;
    codeChangeTool.changed(fileName);
}
exports.notifyLiveReload = notifyLiveReload;

function serveDocs() {
    var server = express();
    server.use(config.APP_BASE, serveStatic(path.resolve(process.cwd(), config.DOCS_DEST)));
    server.listen(config.DOCS_PORT, function () {
        return openResource('http://localhost:' + config.DOCS_PORT + config.APP_BASE);
    });
}
exports.serveDocs = serveDocs;

function serveCoverage() {
    var server = express();
    var compression = require('compression');
    server.use(compression());
    server.use(config.APP_BASE, serveStatic(path.resolve(process.cwd(), 'coverage')));
    server.listen(config.COVERAGE_PORT, function () {
        return openResource('http://localhost:' + config.COVERAGE_PORT + config.APP_BASE);
    });
}
exports.serveCoverage = serveCoverage;

function serveProd() {
    var root = path.resolve(process.cwd(), config.PROD_DEST);
    var server = express();
    var compression = require('compression');
    server.use(compression());
    server.use(config.APP_BASE, serveStatic(root));
    server.use(fallback('index.html', { root: root }));
    server.listen(config.PORT, function () {
        return openResource('http://localhost:' + config.PORT + config.APP_BASE);
    });
}
exports.serveProd = serveProd;
