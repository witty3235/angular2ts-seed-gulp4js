"use strict";
var browserSync = require('browser-sync');
var config = require('../config');
var runServer = function () {
    browserSync.init(config.getPluginConfig('browser-sync'));
};
var listen = function () {
    runServer();
};
exports.listen = listen;
var changed = function (files) {
    if (!(files instanceof Array)) {
        files = [files];
    }
    browserSync.reload(files);
};
exports.changed = changed;
