"use strict";
var fs = require('fs');
var gulp = require('gulp');
var util = require('gulp-util');
var isstream = require('isstream');
var path = require('path');
var tildify = require('tildify');

function loadTasks(_gulp, taskPath) {
    gulp = _gulp;
    util.log('Loading tasks folder', util.colors.yellow(taskPath));
    readDir(taskPath, function (taskname) { return registerTask(taskname, taskPath); });
}
exports.loadTasks = loadTasks;


function registerTask(taskname, taskPath) {
    var TASK = path.join(taskPath, taskname);
    util.log('Registering task', util.colors.yellow(tildify(TASK)));
    gulp.task(taskname, function (done) {
        var task = require(TASK);
        if (task.length > 0) {
            return task(done);
        }
        var taskReturnedValue = task();
        if (isstream(taskReturnedValue)) {
            return taskReturnedValue;
        }
        done();
    });
}

function readDir(root, cb) {
    if (!fs.existsSync(root)) {
        return;
    }
    walk(root);
    function walk(taskPath) {
        var files = fs.readdirSync(taskPath);
        for (var i = 0; i < files.length; i += 1) {
            var file = files[i];
            var curPath = path.join(taskPath, file);
            if (fs.lstatSync(curPath).isFile() && /\.js$/.test(file)) {
                var taskname = file.replace(/\.js$/, '');
                cb(taskname);
            }
        }
    }
}
