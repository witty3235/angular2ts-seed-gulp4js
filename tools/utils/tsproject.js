"use strict";
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();
var tsProject;
function makeTsProject(options) {
    if (!tsProject) {
        var config = Object.assign({
            typescript: require('typescript')
        }, options);
        tsProject = plugins.typescript.createProject('tsconfig.json', config);
    }
    return tsProject;
}
exports.makeTsProject = makeTsProject;
