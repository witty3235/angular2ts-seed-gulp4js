"use strict";

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var path = require('path');
var config = require('../config');
var utils = require('../utils');
var plugins = gulpLoadPlugins();

function watch(taskname) {
    return function () {
        var paths = [
            path.join(config.APP_SRC, '**')
        ].concat(config.TEMP_FILES.map(function (p) { return '!' + p; }));
        plugins.watch(paths, function (e) {
            return gulp.series(taskname, function () { return utils.notifyLiveReload(e); });
        });
    };
}
exports.watch = watch;
