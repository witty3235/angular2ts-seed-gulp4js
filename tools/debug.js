"use strict";

var gulp = require('gulp');
var yargs = require('yargs');
require('../gulpfile');
var TASK = yargs.argv['task'];
if (!TASK) {
    throw new Error('You must specify a task name.');
}
console.log('**********************');
console.log('* angular2ts-seed-gulp4js tools ');
console.log('* debugging task:', TASK);
console.log('**********************');
gulp.start(TASK);
