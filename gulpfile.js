"use strict";
var gulp = require('gulp');
var yargs = require('yargs');
var config = require('./tools/config');
var utils = require('./tools/utils');
utils.loadTasks(gulp, config.TASKS_DIR);

//Subtasks that are primarily used as helper tasks
gulp.task('build.dev', gulp.series('clean.dev', 'tslint', 'css-lint', 'build.assets.dev', 'build.html_css', 'build.js.dev', 'build.index.dev'));
gulp.task('build.dev.watch', gulp.series('build.dev', 'watch.dev'));
gulp.task('build.prod', gulp.series('clean.prod', 'tslint', 'css-lint', 'build.assets.prod', 'build.html_css', 'copy.js.prod', 'build.js.prod', 'build.bundles', 'build.bundles.app', 'build.index.prod'));
gulp.task('build.e2e', gulp.series('clean.dev', 'tslint', 'build.assets.dev', 'build.js.e2e', 'build.index.dev'));
gulp.task('build.test', gulp.series('clean.dev', 'tslint', 'build.assets.dev', 'build.html_css', 'build.js.test', 'build.index.dev'));
gulp.task('build.test.watch', gulp.series('build.test', 'watch.test'));
gulp.task('serve.dev', gulp.series('build.dev', 'server.start', 'watch.dev'));
gulp.task('serve.prod', gulp.series('build.prod', 'server.prod'));

//Tasks that are primarily used from cli
gulp.task('validate:scripts', gulp.series('tslint'));
gulp.task('validate:styles', gulp.series('css-lint'));
gulp.task('validate:all', gulp.series('validate:scripts', 'validate:styles'));

gulp.task('build:all', function() {
    var argv = yargs.argv;
    if(argv.prod) {
        console.log("here");
        return gulp.series('build.prod');
    } else {
        return gulp.series('build.dev');
    }
}());
gulp.task('build:scripts', function() {
    var argv = yargs.argv;
    if(argv.prod) {
        return gulp.series('tslint',
            'copy.js.prod',
            'build.js.prod',
            'build.bundles',
            'build.bundles.app',
            'build.index.prod'
        )();
    } else {
        return gulp.series('tslint',
            'build.js.dev',
            'build.index.dev'
        )();
    }
});
gulp.task('build:styles', function() {
    var argv = yargs.argv;
    if(argv.prod) {
        return gulp.series('css-lint',
            'build.assets.prod',
            'build.html_css'
        )();
    } else {
        return gulp.series('css-lint',
            'build.assets.dev',
            'build.html_css'
        )();
    }
});
gulp.task('build:docs', gulp.series('build.doc'));

gulp.task('clean:all', gulp.series('clean.all'));

gulp.task('test:unit', gulp.series('build.test', 'karma.start'));
gulp.task('test:e2e', gulp.series('build.e2e', 'server.start', 'watch.e2e'));
gulp.task('test:all', gulp.series('test:unit', 'test:e2e'));
