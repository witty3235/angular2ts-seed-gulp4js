# How to start

**Note** that this seed project requires node v4.x.x or higher and npm 2.14.7.

In order to start the seed use:


```bash
git clone --depth 1 https://github.com/mgechev/angular2ts-seed-gulp4js.git
cd angular2ts-seed-gulp4js
# install the project's dependencies
npm install
# watches your files and uses livereload by default
npm start
# api document for the app
# npm run build.docs

# dev build
npm run build.dev
# prod build
npm run build.prod
```

_Does not rely on any global dependencies._

# Table of Content

- [How to start](#how-to-start)
- [Table of Content](#table-of-content)
- [Configuration](#configuration)
- [Tools documentation](#tools-documentation)
- [Running tests](#running-tests)
- [Directory Structure](#directory-structure)
- [License](#license)

# Configuration

Default application server configuration

```js
var PORT             = 5555;
var LIVE_RELOAD_PORT = 4002;
var DOCS_PORT        = 4003;
var APP_BASE         = '/';
```

Configure at runtime

```bash
npm start -- --port 8080 --reload-port 4000 --base /my-app/
```

# Tools documentation

A documentation of the provided tools can be found in [tools/README.md](tools/README.md).

# Running tests

```bash
npm test

# Debug - In two different shell windows
npm run build.test.watch      # 1st window
npm run karma.start           # 2nd window

# code coverage (istanbul)
# auto-generated at the end of `npm test`
# view coverage report:
npm run serve.coverage

# e2e (aka. end-to-end, integration) - In three different shell windows
# Make sure you don't have a global instance of Protractor

# npm run webdriver-update <- You will need to run this the first time
npm run webdriver-start
npm run serve.e2e
npm run e2e

# e2e live mode - Protractor interactive mode
# Instead of last command above, you can use:
npm run e2e.live
```
You can learn more about [Protractor Interactive Mode here](https://github.com/angular/protractor/blob/master/docs/debugging.md#testing-out-protractor-interactively)

# Directory Structure

```
.
├── LICENSE
├── README.md
├── gulpfile.js                <- configuration of the gulp tasks
├── karma.conf.js              <- configuration of the test runner
├── package.json               <- dependencies of the project
├── protractor.conf.js         <- e2e tests configuration
├── src                        <- source code of the application
│   └── client
│       ├── app
│       │   ├── +about
│       │   │   ├── about.component.scss
│       │   │   ├── about.component.e2e-spec.ts
│       │   │   ├── about.component.html
│       │   │   ├── about.component.spec.ts
│       │   │   ├── about.component.ts
│       │   │   └── index.ts
│       │   ├── +home
│       │   │   ├── home.component.scss
│       │   │   ├── home.component.e2e-spec.ts
│       │   │   ├── home.component.html
│       │   │   ├── home.component.spec.ts
│       │   │   ├── home.component.ts
│       │   │   └── index.ts
│       │   ├── app.component.e2e-spec.ts
│       │   ├── app.component.html
│       │   ├── app.component.spec.ts
│       │   ├── app.component.ts
│       │   ├── hot_loader_main.ts
│       │   ├── main.ts
│       │   └── shared
│       │       ├── index.ts
│       │       ├── name-list
│       │       │   ├── index.ts
│       │       │   ├── name-list.service.spec.ts
│       │       │   └── name-list.service.ts
│       │       ├── navbar
│       │       │   ├── index.ts
│       │       │   ├── navbar.component.scss
│       │       │   ├── navbar.component.html
│       │       │   └── navbar.component.ts
│       │       └── toolbar
│       │           ├── index.ts
│       │           ├── toolbar.component.scss
│       │           ├── toolbar.component.html
│       │           └── toolbar.component.ts
│       ├── assets
│       │   └── svg
│       │       └── more.svg
│       ├── css
│       │   ├── main.scss
│       │   └── main.css
│       ├── index.html
│       ├── tsconfig.json
│       └── typings.d.ts
├── test-main.js               <- testing configuration
├── tools
│   ├── README.md              <- build documentation
│   ├── config
│   │   ├── config.js          <- extended configuration
│   │   └── config.default.js  <- default configuration
│   ├── config.js              <- exported configuration (merge both seed.config and project.config, project.config overrides seed.config)
│   ├── debug.js
│   ├── manual_typings          <- manual ambient typings for the project
│   |   ├── angular2-hot-loader.d.ts
│   │   └── sample.package.d.ts
│   ├── tasks                  <- gulp tasks
│   │   ├── build.assets.dev.js
│   │   ├── build.assets.prod.js
│   │   ├── build.bundles.app.js
│   │   ├── build.bundles.js
│   │   ├── build.docs.js
│   │   ├── build.html_css.js
│   │   ├── build.index.dev.js
│   │   ├── build.index.prod.js
│   │   ├── build.js.dev.js
│   │   ├── build.js.e2e.js
│   │   ├── build.js.prod.js
│   │   ├── build.js.test.js
│   │   ├── check.versions.js
│   │   ├── clean.all.js
│   │   ├── clean.dev.js
│   │   ├── clean.prod.js
│   │   ├── copy.js.prod.js
│   │   ├── css-lint.js
│   │   ├── e2e.js
│   │   ├── generate.manifest.js
│   │   ├── karma.start.js
│   │   ├── sample.task.js
│   │   ├── serve.coverage.js
│   │   ├── serve.docs.js
│   │   ├── server.prod.js
│   │   ├── server.start.js
│   │   ├── tslint.js
│   │   ├── watch.dev.js
│   │   ├── watch.e2e.js
│   │   ├── watch.test.js
│   │   └── webdriver.js
│   ├── utils                  <- build utils
│   │   ├── project            <- project specific gulp utils
│   │   │   └── sample_util.js
│   │   ├── project.utils.js
│   │   ├── seed               <- seed specific gulp utils
│   │   │   ├── clean.js
│   │   │   ├── code_change_tools.js
│   │   │   ├── server.js
│   │   │   ├── tasks_tools.js
│   │   │   ├── tsproject.js
│   │   │   └── watch.js
│   │   └── seed.utils.js
│   └── utils.js
├── tsconfig.json              <- configuration of the typescript project (ts-node, which runs the tasks defined in gulpfile.js)
├── tslint.json                <- tslint configuration
├── typings                    <- typings directory. Contains all the external typing definitions defined with typings
└── typings.json
```

# License

MIT
